<?php
/**
 * Bright Spot Studio - Broker Schedules
 *
 * @package     Bright Spot Studio - Broker Schedules
 * @author      Michal Bluma
 * @license     GPL-2.0+
 *
 * @wordpress-plugin
 * Plugin Name: Bright Spot Studio - Broker Schedules
 * Plugin URI:  http://monassurancehypotheque.com
 * Description: Ability to define different CC'ed recipients for Gravity Forms contact form
 * Version:     1.0.0
 * Author:      Michal Bluma for Bright Spot Studio
 * Author URI:  https://isotrope.net
 * Text Domain: bss-broker-schedules
 * Domain Path: /languages
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */


add_filter( 'init', 'bss_broker_schedules_load_plugin_textdomain' );
function bss_broker_schedules_load_plugin_textdomain() {
	load_plugin_textdomain( 'bss-broker-schedules', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}


/*
 * If Gravity Forms isn't active, no use in going on
 */

add_filter( 'init', 'bss_maybe_check_for_gf' );
function bss_maybe_check_for_gf() {
	if ( ! class_exists( 'GFCommon' ) ) {
		add_action( 'admin_notices', 'bss_gf_missing_notice' );
	}
}

function bss_gf_missing_notice() {
	?>
	<div class="error notice">
		<p><?php _e( 'Gravity Forms doesn\'t seem to be active. The Broker Schedules plugin relies on it.', 'bss-broker-schedules' ); ?></p>
	</div>
	<?php
}


/*
 * This plugin relies on CMB2 for its custom fields
 * https://github.com/WebDevStudios/CMB2
 */
add_filter( 'init', 'bss_maybe_check_for_cmb2' );
function bss_maybe_check_for_cmb2() {
	if ( ! defined( 'CMB2_LOADED' ) ) {
		add_action( 'admin_notices', 'bss_cmb2_missing_notice' );
	}
}

function bss_cmb2_missing_notice() {
	?>
	<div class="error notice">
		<p><?php _e( 'CMB2 doesn\'t seem to be active. The Broker Schedules plugin relies on it.', 'bss-broker-schedules' ); ?></p>
	</div>
	<?php
}


/*
 * Utilities used throughout
 */
include_once( 'modules/utilities.php' );


/*
 * CPTs for the Brokers
 */
include_once( 'modules/cpts.php' );
$bss_broker_schedules_cpts = new Bss_Broker_Schedules_Cpts();
$bss_broker_schedules_cpts->init();


/*
 * Custom Fields for the Brokers CPT
 */
include_once( 'modules/custom-fields.php' );
$bss_broker_schedules_customfields = new Bss_Broker_Schedules_CustomFields();
$bss_broker_schedules_customfields->init();


/*
 * Gravity Forms hooking for the Brokers
 */

include_once( 'modules/gf-hooks.php' );
$bss_broker_schedules_gf_hooks = new Bss_Broker_Schedules_Gf_Hooks();
$bss_broker_schedules_gf_hooks->init();
