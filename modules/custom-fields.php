<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


/**
 */
class Bss_Broker_Schedules_CustomFields {
	/**
	 * Option key, and option page slug
	 * @var string
	 */
	/**
	 * Options page metabox id
	 * @var string
	 */

	public $fields_prefix = 'mah_';
	protected $options_page = '';
	private $metabox_id = 'mah_options_schedule_metabox';
	private $key = 'bss-broker-schedules';

	public function __construct() {
		// Empty
		// Using init()
	}

	public function init() {

		// Add an email field to the Brokers CPT
		add_action( 'cmb2_admin_init', array( $this, 'register_broker_email_metabox' ) );


		add_action( 'admin_menu', array( $this, 'add_options_page' ) );
		add_action( 'cmb2_admin_init', array( $this, 'add_options_page_metabox' ) );

	}


	public function register_broker_email_metabox() {

		$cmb_email = new_cmb2_box( array(
			'id'           => $this->fields_prefix . 'email_box',
			'title'        => __( 'Email', 'bss-broker-schedules' ),
			'object_types' => array( 'bss_brokers', 'bss-broker-schedules' ), // Post type
		) );

		$cmb_email->add_field( array(
			'name' => __( 'Email', 'bss-broker-schedules' ),
			'id'   => $this->fields_prefix . 'email-field',
			'type' => 'text_email',
		) );
	}


	public function add_options_page() {
		$this->options_page = add_menu_page(
			__( 'Brokers Schedules', 'bss-broker-schedules' ), // $page_title
			__( 'Brokers Schedules', 'bss-broker-schedules' ), // $menu_title
			'edit_posts', //$capability
			$this->key, // $menu_slug
			array( $this, 'admin_schedule_options_page_display' ), // callback function
			'dashicons-calendar-alt', // $icon_url
			21 // $icon_url
		);


		// Include CMB CSS in the head to avoid FOUC
		add_action( "admin_print_styles-{$this->options_page}", array( 'CMB2_hookup', 'enqueue_cmb_css' ) );
	}


	public function admin_schedule_options_page_display() {
		$which_forms_metabox      = cmb2_metabox_form( $this->metabox_id . '-which-forms', $this->key, array( 'echo' => false ) );
		$brokers_repeater_metabox = cmb2_metabox_form( $this->metabox_id, $this->key, array( 'echo' => false ) );
		?>
		<style>
			.cmb2-metabox .cmb2-id-mah-repeatable-broker-schedules {
				max-width: none;
			}

			.cmb2-id-mah-repeatable-broker-schedules .inside {
				display: flex;
			}

			.cmb2-id-mah-repeatable-broker-schedules .inside .cmb-row {
				flex-grow: 1;
			}

			.schedule-table {
				position: relative;
			}

			.day-headers:after,
			.schedule-table .tr:after {
				content: " "; /* Older browser do not support empty content */
				visibility: hidden;
				display: block;
				height: 0;
				clear: both;
			}

			.day-headers .blank-cell,
			.schedule-table .header-cell,
			.schedule-table .spacer-cell {
				float: left;
			}

			.schedule-table .blank-cell,
			.schedule-table .header-cell {
				height: 20px;
				text-align: center;
				width: <?php echo ( 1 / 8 * 100 ); ?>%;
			}

			.schedule-table .spacer-cell {
				position: relative;
			}

			.schedule-table .spacer-cell:after,
			.time-headers .header-cell:after {
				background: rgba(0, 0, 0, 0.1);
				content: ' ';
				position: absolute;
				left: 100%;
				top: 0;
				height: 100%;
				width: 1px;
			}

			.schedule-table .tr,
			.day-headers {
				border-bottom: 1px solid rgba(0, 0, 0, 0.3);
				border-left: 1px solid rgba(0, 0, 0, 0.3);
			}

			.schedule-table .tr,
			.day-headers {
				width: <?php echo ( 8 / 9 * 100 ); ?>%;
			}

			.time-headers .header-cell {
				position: relative;
			}

			.schedule-table .schedule-cell {
				border: 1px solid rgba(0, 255, 0, 1);
				display: flex;
				align-items: center;
				justify-content: center;

				background: rgba(0, 255, 0, 0.8);

				position: absolute;
				width: <?php echo ( 1 / 9 * 100 ); ?>%;
			}

			.schedule-table .schedule-cell:hover {
				border-color: black;
				z-index: 50;
			}

		</style>

		<div class="wrap">
			<h2><?php _e( 'Utilities', 'bss-broker-schedules' ); ?></h2>
			<div><?php _e( 'The server thinks that it is currently: ', 'bss-broker-schedules' );
				echo date( 'g:i A', current_time( 'timestamp' ) );
				echo ' (' . date_i18n( get_option( 'time_format' ) ) . ')'; ?></div>

			<div>
				<?php
				_e( 'Brokers currently available: ', 'bss-broker-schedules' );
				echo Bss_Broker_Schedules_Utilities::get_available_brokers_list();
				?>

			</div>

		</div>
		

		<div class="wrap cmb2-options-page <?php echo $this->key; ?>">
			<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
			<?php echo $which_forms_metabox; ?>
		</div>

		<div class="wrap cmb2-options-page <?php echo $this->key; ?>">
			<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
			<?php echo $brokers_repeater_metabox; ?>
		</div>


		<?php echo Bss_Broker_Schedules_Utilities::generate_schedule_table(); ?>

		<?php
	}

	function add_options_page_metabox() {
		// hook in our save notices
		add_action( "cmb2_save_options-page_fields_{$this->metabox_id}", array( $this, 'settings_notices' ), 10, 2 );
		add_action( "cmb2_save_options-page_fields_" . $this->metabox_id . "-which-forms", array(
			$this,
			'settings_notices'
		), 10, 2 );

		$cmb_group_forms = new_cmb2_box( array(
			'id'      => $this->metabox_id . '-which-forms',
			'title'   => __( 'Which forms should this be applied to', 'bss-broker-schedules' ),
			'show_on' => array(
				// These are important, don't remove
				'key'   => 'options-page',
				'value' => array( $this->key, )
			),
		) );

		$forms_cmb_radio = $cmb_group_forms->add_field( array(
			'id'               => $this->fields_prefix . 'gf-forms',
			'type'             => 'multicheck',
			'show_option_none' => false,
			'options'          => $this->get_checkbox_form_options(),
		) );


		/**
		 * Repeatable Field Groups
		 */
		$cmb_group = new_cmb2_box( array(
			'id'      => $this->metabox_id,
			'title'   => __( 'All Schedules', 'bss-broker-schedules' ),
			'show_on' => array(
				// These are important, don't remove
				'key'   => 'options-page',
				'value' => array( $this->key, )
			),
		) );

		// $group_field_id is the field id string, so in this case: $prefix . 'demo'
		$group_field_id = $cmb_group->add_field( array(
			'id'          => $this->fields_prefix . 'repeatable_broker-schedules',
			'type'        => 'group',
			'description' => __( 'Manage all your broker schedules', 'bss-broker-schedules' ),
			'options'     => array(
				'group_title'   => __( 'Entry {#}', 'bss-broker-schedules' ), // {#} gets replaced by row number
				'add_button'    => __( 'Add Another Broker Schedule', 'bss-broker-schedules' ),
				'remove_button' => __( 'Remove Broker Schedule', 'bss-broker-schedules' ),
				'sortable'      => true, // beta
				// 'closed'     => true, // true to have the groups closed by default
			),
		) );


		$cmb_group->add_group_field( $group_field_id, array(
			'name'             => __( 'Broker', 'bss-broker-schedules' ),
			'desc'             => __( 'Select a broker', 'bss-broker-schedules' ),
			'id'               => 'broker_select',
			'type'             => 'select',
			'show_option_none' => true,
			'default'          => 'custom',
			'options'          => Bss_Broker_Schedules_Utilities::get_all_brokers_for_select(),
		) );


		$cmb_group->add_group_field( $group_field_id, array(
			'name'             => __( 'Day of the week', 'bss-broker-schedules' ),
			'desc'             => __( 'Select a day', 'bss-broker-schedules' ),
			'id'               => 'day_select',
			'type'             => 'select',
			'show_option_none' => false,
			'options'          => Bss_Broker_Schedules_Utilities::get_dow_lookup(),
		) );

		$cmb_group->add_group_field( $group_field_id, array(
			'name' => __( 'Time start', 'bss-broker-schedules' ),
			'id'   => 'time_start',
			'type' => 'text_time',
			//'time_format' => 'H:i:s A',
		) );

		$cmb_group->add_group_field( $group_field_id, array(
			'name' => __( 'Time end', 'bss-broker-schedules' ),
			'id'   => 'time_end',
			'type' => 'text_time',
			// 'time_format' => 'H:i:s A',
		) );
	}


	public function settings_notices( $object_id, $updated ) {
		if ( $object_id !== $this->key || empty( $updated ) ) {
			return;
		}
		add_settings_error( $this->key . '-notices', '', __( 'Settings updated.', 'bss-broker-schedules' ), 'updated' );
		settings_errors( $this->key . '-notices' );
	}


	public function get_checkbox_form_options() {
		// http://www.tcbarrett.com/2013/03/how-to-get-a-list-of-all-your-gravity-forms/#.V9VokK08qLU
		$available_forms = RGFormsModel::get_forms( null, 'title' );
		$form_options    = array();

		foreach ( $available_forms as $form ) {
			$form_options[ $form->id ] = $form->title;
		}


		return $form_options;

	}

}
