<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


class Bss_Broker_Schedules_Gf_Hooks {

	public function __construct() {
		// Empty
	}

	public function init() {
		add_filter( 'gform_notification', array( $this, 'add_brokers_cc' ), 10, 3 );

	}


	public function add_brokers_cc( $notification, $form, $entry ) {
		$option_name = 'bss-broker-schedules';
		$option      = get_option( $option_name, false );

		// bail if the option doesn't exist
		if ( empty( $option ) || empty ( $option['mah_gf-forms'] ) ) {
			return $notification;
		}

		// Bail if we don't care about this form
		if ( ! in_array( $form['id'], $option['mah_gf-forms'] ) ) {
			return $notification;
		}


		// Still here? We want to do something with this form
		$available_brokers = Bss_Broker_Schedules_Utilities::get_available_brokers();
		$brokers_to        = array();
		if ( ! empty( $available_brokers ) ) {
			foreach ( $available_brokers as $broker ) {
				$brokers_to[] = $broker['email'];
			}
		}

		$existing_to = explode( ';', $notification['to'] );

		if ( count( $brokers_to ) > 0 ) {
			$notification['to'] = GFCommon::implode_non_blank( ',', array_merge( $existing_to, $brokers_to ) );
		}

		// Add a note about who this was sent to
		RGFormsModel::add_note(
			$entry['id'], // $entry id
			0, // Note User ID
			_x( 'NOTIFICATIONS SYSTEM', 'User name for notes added to the Gform entry', 'bss-broker-schedules' ), // Note Username
			sprintf( _x( 'Notifications sent out to %1$s', 'Message added inside note when entry is submitted', 'bss-broker-schedules' ), $notification['to'] ) // Note Message
		);

		return $notification;
	}

}