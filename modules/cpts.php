<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}



class Bss_Broker_Schedules_Cpts {

	public function __construct() {
		// Empty
		// Using init()
	}

	public function init() {

		// Registers post_types and custom columns
		add_action( 'init', array( $this, 'setup_post_types' ) );

	}

	public function setup_post_types() {
		$this->register_cpt_brokers();
	}


	public function register_cpt_brokers() {
		$labels  = array(
			'name'                  => _x( 'Brokers', 'Post Type General Name', 'bss-broker-schedules' ),
			'singular_name'         => _x( 'Broker', 'Post Type Singular Name', 'bss-broker-schedules' ),
			'menu_name'             => __( 'Brokers', 'bss-broker-schedules' ),
			'name_admin_bar'        => __( 'Brokers', 'bss-broker-schedules' ),
			'all_items'             => __( 'All Brokers', 'bss-broker-schedules' ),
			'add_new_item'          => __( 'Add New Broker', 'bss-broker-schedules' ),
			'add_new'               => _x( 'Add New', 'Add New -> Broker', 'bss-broker-schedules' ),
			'new_item'              => __( 'New Broker', 'bss-broker-schedules' ),
			'edit_item'             => __( 'Edit Broker', 'bss-broker-schedules' ),
			'update_item'           => __( 'Update Broker', 'bss-broker-schedules' ),
			'view_item'             => __( 'View Broker', 'bss-broker-schedules' ),
			'search_items'          => __( 'Search Brokers', 'bss-broker-schedules' ),
			'not_found'             => __( 'Not found', 'bss-broker-schedules' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'bss-broker-schedules' ),
			'featured_image'        => __( 'Featured Image', 'bss-broker-schedules' ),
			'set_featured_image'    => __( 'Set featured image', 'bss-broker-schedules' ),
			'remove_featured_image' => __( 'Remove featured image', 'bss-broker-schedules' ),
			'use_featured_image'    => __( 'Use as featured image', 'bss-broker-schedules' ),
			'uploaded_to_this_item' => __( 'Uploaded to this broker', 'bss-broker-schedules' ),
			'items_list'            => __( 'Brokers list', 'bss-broker-schedules' ),
			'items_list_navigation' => __( 'Brokers list navigation', 'bss-broker-schedules' ),
			'filter_items_list'     => __( 'Filter brokers list', 'bss-broker-schedules' ),
		);

		$args    = array(
			'label'               => __( 'Brokers', 'bss-broker-schedules' ),
			'description'         => __( 'Brokers CPT for email dispatch scheduling', 'bss-broker-schedules' ),
			'labels'              => $labels,
			'supports'            => array(
				'title',
				'revisions',
			),
			'hierarchical'        => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 20,
			'menu_icon'           => 'dashicons-calendar-alt',
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => false,
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'rewrite'             => false,
			'capability_type'     => 'page',
		);
		register_post_type( 'bss_brokers', $args );
	}


}
