<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


class Bss_Broker_Schedules_Utilities {

	public function __construct() {
		// Empty
	}

	public static $dow_lookup = array();


	public static function get_all_brokers_for_select() {
		$brokers_out = array();

		$args = array(
			'post_type'      => 'bss_brokers',
			'posts_per_page' => 9999,
			'order'          => 'ASC',
			'orderby'        => 'title',
		);

		$brokers_query = new WP_Query( $args );

		if ( $brokers_query->have_posts() ) {
			while ( $brokers_query->have_posts() ) {
				$brokers_query->the_post();
				$brokers_out[ get_the_ID() ] = get_the_title();
			}
		}

		wp_reset_postdata();

		return $brokers_out;
	}


	public static function get_dow_lookup() {
		return array(
			'monday'    => __( 'Monday', 'bss-broker-schedules' ),
			'tuesday'   => __( 'Tuesday', 'bss-broker-schedules' ),
			'wednesday' => __( 'Wednesday', 'bss-broker-schedules' ),
			'thursday'  => __( 'Thursday', 'bss-broker-schedules' ),
			'friday'    => __( 'Friday', 'bss-broker-schedules' ),
			'saturday'  => __( 'Saturday', 'bss-broker-schedules' ),
			'sunday'    => __( 'Sunday', 'bss-broker-schedules' ),
		);
	}

	public static function get_dow_lookup_num() {
		return array(
			1 => 'monday',
			2 => 'tuesday',
			3 => 'wednesday',
			4 => 'thursday',
			5 => 'friday',
			6 => 'saturday',
			7 => 'sunday',
		);
	}

	public static function get_pretty_dow( $day_key ) {

		$pretty_day = '';

		$dows = self::get_dow_lookup();

		if ( ! empty( $dows[ $day_key ] ) ) {
			$pretty_day = $dows[ $day_key ];
		}

		return $pretty_day;
	}


	public static function get_brokers_lookup() {

		$brokers_out = array();

		$args = array(
			'post_type'      => 'bss_brokers',
			'posts_per_page' => 9999,
			'order'          => 'ASC',
			'orderby'        => 'title',
		);

		$brokers_query = new WP_Query( $args );

		if ( $brokers_query->have_posts() ) {
			while ( $brokers_query->have_posts() ) {
				$brokers_query->the_post();


				$broker_id    = get_the_ID();
				$broker_name  = get_the_title();
				$broker_email = get_post_meta( $broker_id, 'mah_email-field', true );

				$brokers_out[ get_the_ID() ] = array(
					'id'    => $broker_id,
					'ID'    => $broker_id,
					'title' => $broker_name,
					'name'  => $broker_name,
					'email' => $broker_email,
				);

			}
		}

		wp_reset_postdata();


		return $brokers_out;

	}


	public static function get_group_schedules() {
		$option_name  = 'bss-broker-schedules';
		$return_array = array();

		$option = get_option( $option_name, false );

		// bail if the option doesn't exist
		if ( empty( $option ) ||  empty ( $option['mah_repeatable_broker-schedules'] ) ) {
			return $return_array;
		}

		$schedules = $option['mah_repeatable_broker-schedules'];

		$brokers_lookup = self::get_brokers_lookup();
		$dow_lookup     = self::get_dow_lookup();

		// First keys will be DOW
		foreach ( $dow_lookup as $day => $pretty_day ) {
			$return_array[ $day ] = array();
		}

		// Add in time values
		foreach ( $schedules as $schedule ) {
			// Doesn't seem to be a valid Broker
			if ( empty( $schedule['broker_select'] ) || empty( $brokers_lookup[ $schedule['broker_select'] ] ) ) {
				continue;
			}

			$broker_id     = $brokers_lookup[ $schedule['broker_select'] ]['id'];
			$broker_name   = $brokers_lookup[ $schedule['broker_select'] ]['name'];
			$broker_email  = $brokers_lookup[ $schedule['broker_select'] ]['email'];
			$dow           = $schedule['day_select'];
			$time_start_hi = date( 'H:i', strtotime( $schedule['time_start'] ) );
			$time_start_ts = strtotime( 'today' . $schedule['time_start'] );
			$time_end_hi   = date( 'H:i', strtotime( $schedule['time_end'] ) );
			$time_end_ts   = strtotime( 'today' . $schedule['time_end'] );

			$day_start_ts = strtotime( 'today' );
			$day_end_ts   = strtotime( 'tomorrow' );


			if ( array_key_exists( $dow, $return_array ) ) {
				$return_array[ $dow ][] = array(
					'id'            => $broker_id,
					'ID'            => $broker_id,
					'name'          => $broker_name,
					'title'         => $broker_name,
					'email'         => $broker_email,
					'dow'           => $dow,
					'time_start_hi' => $time_start_hi,
					'time_start_ts' => $time_start_ts,
					'time_end_hi'   => $time_end_hi,
					'time_end_ts'   => $time_end_ts,
					'day_start_ts'  => $day_start_ts,
					'day_end_ts'    => $day_end_ts,

					'time_start_day_fraction' => ( $time_start_ts - $day_start_ts ),
					'time_end_day_fraction'   => ( $time_end_ts - $day_start_ts ),

					'full_day' => $day_end_ts - $day_start_ts,

					'pretty_time_start' => date( 'Y-m-d H:i', $time_start_ts ),
					'pretty_time_end'   => date( 'Y-m-d H:i', $time_end_ts ),
					'pretty_day_start'  => date( 'Y-m-d H:i', $day_start_ts ),
					'pretty_day_end'    => date( 'Y-m-d H:i', $day_end_ts ),
				);
			}

		}


		return $return_array;
	}


	public static function get_available_brokers( $timestamp = '' ) {


		$now               = '';
		$schedules         = self::get_group_schedules();
		$dows              = self::get_dow_lookup_num();
		$dow_lookup_day    = "";
		$brokers_today     = array();
		$available_brokers = array();


		if ( ! empty( $timestamp ) ) {
			$now            = date( 'U', $timestamp );
			$dow_lookup_day = $dows[ date( 'N', $timestamp ) ];
		} else {
			$now            = current_time( 'timestamp' );
			$dow_lookup_day = $dows[ date( 'N' ) ];
		}


		if ( ! empty( $schedules[ $dow_lookup_day ] ) ) {
			$brokers_today = $schedules[ $dow_lookup_day ];

			foreach ( $brokers_today as $broker ) {
				if ( ! empty( $broker['time_start_ts'] ) && ! empty( $broker['time_end_ts'] ) ) {
					if ( $broker['time_start_ts'] <= $now && $broker['time_end_ts'] > $now ) {
						$available_brokers[] = $broker;
					}
				}
			}
		}


		return $available_brokers;
	}


	public static function get_available_brokers_list( $timestamp = '' ) {
		$available_brokers = self::get_available_brokers( $timestamp );

		if ( count( $available_brokers ) < 1 ) {
			return __( 'No available brokers at this time', 'bss-broker-schedules' );
		}

		$list_out = array();
		foreach ( $available_brokers as $broker ) {
			$broker_info = '<a href="' . esc_url( get_edit_post_link( $broker['id'] ) ) . '" ';
			$broker_info .= ' title="' . __( 'Clicking this will open a new tab to edit this broker', 'bss-broker-schedules' ) . '" target="_blank">';
			$broker_info .= $broker['name'];
			$broker_info .= '</a>';

			$list_out[] = $broker_info;
		}


		return join( ', ', $list_out );
	}


	public static function generate_schedule_table() {

		$html_out = '<div class="wrap">' . "\n";
		$html_out .= "\t" . '<div class="schedule-table">' . "\n";

		$dow_lookup = self::get_dow_lookup();
		$schedules  = self::get_group_schedules();

		$cell_height       = 21; // include border
		$cell_height_style = 'height: ' . $cell_height . 'px';

		$arr_hours = array();
		for ( $i = 0; $i < 24; $i ++ ) {
			$arr_hours[] = str_pad( strval( $i ), 2, "0", STR_PAD_LEFT ) . ":00";
			$arr_hours[] = str_pad( strval( $i ), 2, "0", STR_PAD_LEFT ) . ":30";
		}

		// Header rows
		$html_out .= "\t\t" . '<div class="day-headers">' . "\n";
		$html_out .= "\t\t\t" . '<div class="blank-cell">&nbsp;</div>' . "\n"; // for "times" column

		foreach ( $dow_lookup as $dow => $dow_pretty ) {
			$html_out .= "\t\t\t" . '<div class="header-cell">' . $dow_pretty . '<div class="vertical"></div></div>' . "\n";
		}
		$html_out .= "\t\t" . '</div>' . "\n";


		$html_out .= "\t\t" . '<div class="time-headers">' . "\n";


		$seven_blank_divs = '';
		for ( $i = 0; $i < 7 ; $i ++ ) {
		$seven_blank_divs .= '<div class="spacer-cell blank-cell"></div>';
		}
		foreach ( $arr_hours as $time ) {
			$html_out .= "\t\t\t" . '<div class="tr"><div class="header-cell">' . $time . '</div>' . $seven_blank_divs . '</div>' . "\n";
		}
		$html_out .= "\t\t" . '</div>' . "\n";


		// schedules
		$day_counter = 1;
		foreach ( $schedules as $schedule_day ) {

			foreach ( $schedule_day as $schedule_entry ) {

				$left   = $day_counter / 9 * 100;
				$top    = ($schedule_entry['time_start_day_fraction']  /  $schedule_entry['full_day'] * ( $cell_height * 48 ) ) + $cell_height - 1;
				$bottom = ( $schedule_entry['full_day'] - $schedule_entry['time_end_day_fraction'] ) / $schedule_entry['full_day']  * ( $cell_height * 48 )  ;

				$entry_info_to_display = $schedule_entry['time_start_hi'] . ' - ' . $schedule_entry['time_end_hi'];

				$html_out .= "\t\t\t" . '<div class="schedule-cell" style="left: ' . $left . '%; top: ' . $top . 'px; bottom: ' . $bottom . 'px;" title="' . esc_attr( $schedule_entry['name'] . ' - ' . $dow_lookup[ $schedule_entry['dow'] ] . ' - ' . $entry_info_to_display ) . '">' . $schedule_entry['name'] . '<br />' . $entry_info_to_display . '</div>' . "\n";
			}
			$day_counter ++;
		}


		$html_out .= "\t\t" . '</div>' . "\n";
		$html_out .= '</div>' . "\n";

		return $html_out;
	}

}